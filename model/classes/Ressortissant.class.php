<?php

class Ressortissant{
    public $numRessortissant;
    public $Sexe;
    public $nomRessortissant;
    public $prénomRessortissant;
    public $naissanceRessortissant;
    public $adresseRessortissant;
    public $dateInscription;
    public $codeDécès;
    public $carteConsul;
    public $codePostal;
    public $numVille;
    public $numCarteConsul;
    public $dateExpCarte;
     // Constructeur
     public function __construct(array $donnees)
     {
         if (!empty($donnees)) // Si on a spécifié des valeurs, alors on hydrate l'objet.
         {
           $this->hydrate($donnees);
         }      
     }
  
     // Hydratation
     public function hydrate(array $donnees)
     {
         foreach ($donnees as $key => $value)
         {
           $method = 'set'.ucfirst($key);
                    
           if (method_exists($this, $method))
           {
             $this->$method($value);
           }
         }
     }  
    
     
}
?>