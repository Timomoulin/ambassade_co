<?php
require __DIR__."/../classes/Ressortissant.class.php";
class BDD{
    private $conn;
    public function __construct()
    {
        $this->conn=new PDO("mysql:host=localhost;dbname=ambassade_co", "root", "",array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
        $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
       
      
    }
    public function getAllRessortissant()
    {
        try{
            $stmt = $this->conn->prepare("SELECT * FROM ressortissants");
            $stmt->execute();
            $test=array(array());
            $resultat = ($stmt->fetchAll(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, 'Ressortissant', $test));
            return $resultat;
        }
        catch(PDOException $e) {
            echo "Error: " . $e->getMessage();
        }
    }
}

$BDDTest=new BDD();
$BDDTest->getAllRessortissant();

?>