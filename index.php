<?php
require('controller/controller.php');
/* index.php Est le routeur il va :
*   - Regarder quelle est la valeur du param action
*   - Generalement il vas allez chercher des données dans le model
*   - Et utiliser invoquer (appeler) une fonction du controlleur en lui passant des données (du model) si besoin
*/
try{
    if (isset($_GET['action'])) {
        if ($_GET['action'] == 'Ressortissants') {
            voirRessortissants();
        }
        else{
            throw new Exception ("Pas de page pour cette action");
        }
    }
    else {
        page1();
    }
}
catch(Exception $e){
    echo 'Erreur : ' . $e->getMessage();
}

?>